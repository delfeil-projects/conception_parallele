#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <chrono>

using namespace std;

int flow;

condition_variable cond_var_barrage;
mutex barrage_mutex;
mutex affiche_mutex;

void riverControl() {
	while(true) {
		if(flow >=5) {
			cond_var_barrage.notify_one();
			affiche_mutex.lock();
			cout<<"Il faut ouvrir les vannes"<<endl;
			affiche_mutex.unlock();
		}
		flow++;
		affiche_mutex.lock();
		cout<<"flux de la rivière: "<<flow<<endl;
		affiche_mutex.unlock();
		this_thread::sleep_for(chrono::milliseconds(1000));
	}
}

void barrage() {
	while(true) {
		unique_lock<mutex> lock(barrage_mutex);
		cond_var_barrage.wait(lock);
		flow =0;
		affiche_mutex.lock();
		cout<<"Ouverture des vannes"<<endl;
		affiche_mutex.unlock();
	}	
}

int main() {
	thread t_river(riverControl);
	thread t_barrage(barrage);
	t_river.join();
	t_barrage.join();
	return 0;
}