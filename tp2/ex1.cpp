#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <mutex>
#include "compteur.h"

using namespace std;

void compt(int nCompt, string comptName) {
	Compteur cpt(nCompt, comptName);
	// cpt.compter();
	cpt.compterV2();
}

int main() {
	// Compteur::pos=1;
	int nbThreads = 3;
	int nCompt = 10;
	string comptName = "Franklin ";
	thread threadTable[nbThreads];
	for(int i=0; i<nbThreads; i++) {
		threadTable[i] = thread(compt, nCompt, comptName + to_string(i));
	}

	for(int i=0; i<nbThreads; i++) {
		threadTable[i].join();
	}
	return 0;
}