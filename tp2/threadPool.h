#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <stack>
#include <vector>

using namespace std;

class threadPool
{
	stack<thread*> pool;
	int nbThreads;
	public:
		threadPool(int nbThread);
		void push(thread*);
		void pop();
		bool empty();
		void lancer();
		static int nbActifThreads;
		static mutex mutex_notif;
		static mutex pool_mutex;
		static mutex mutex_thread;
		static condition_variable cond_var;
};