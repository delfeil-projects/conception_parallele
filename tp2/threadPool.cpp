#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include "threadPool.h"

using namespace std;

mutex threadPool::pool_mutex;
mutex threadPool::mutex_notif;
mutex threadPool::mutex_thread;
condition_variable threadPool::cond_var;
int threadPool::nbActifThreads = 0;

threadPool::threadPool(int nbThread) {
	nbThreads = nbThread;
	nbActifThreads = 0;
}

void threadPool::push(thread* t) {
	pool_mutex.lock();
	pool.push(t);
	pool_mutex.unlock();
}

void threadPool::pop() {
	pool_mutex.lock();
	pool.pop();
	pool_mutex.unlock();
}

bool threadPool::empty() {
	return pool.empty();
}

void threadPool::lancer() {
	while(!empty()) {
		while(!empty() && nbActifThreads<nbThreads) {
			pop();
			cond_var.notify_one();
		}
	}
}