#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <string>
#include <condition_variable>
#include "compteur.h"

mutex affiche_mutex;
condition_variable cond_var;

int Compteur::pos = 1;

Compteur::Compteur(int nb, string nomCompt) {
	n = nb;
	nom = nomCompt;
}

void Compteur::compter() {
	for(int i=1; i<n+1; i++) {
		affiche_mutex.lock();
		cout<<nom<<" : "<<i<<endl;
		affiche_mutex.unlock();
		int randomT = rand() % 2001;
		this_thread::sleep_for(chrono::milliseconds(randomT));
	}
	affiche_mutex.lock();
	cout<<nom<<" a fini de compter jusqu'à "<<n<<" à la position "<<pos++<<endl;
	affiche_mutex.unlock();
}

void Compteur::compterV2() {
	for(int i=1; i<n+1; i++) {
		unique_lock<mutex> lock(affiche_mutex);
		int randomT = rand() % 2001;
		cond_var.wait_for(lock, chrono::milliseconds(randomT));
		cout<<nom<<" : "<<i<<endl;
		// this_thread::sleep_for(chrono::milliseconds(randomT));
		cond_var.notify_one();
	}
	int randomT = rand() % 2001;
	unique_lock<mutex> lock(affiche_mutex);
	cond_var.wait_for(lock, chrono::milliseconds(randomT));
	cout<<nom<<" a fini de compter jusqu'à "<<n<<" à la position "<<pos++<<endl;
	cond_var.notify_one();
}

int Compteur::getN() {
	return n;
}