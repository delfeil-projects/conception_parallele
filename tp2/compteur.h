#include <iostream>
#include <string>
#include <thread>
#include <mutex>

using namespace std;

class Compteur
{
	int n;
	string nom;
	public:
		Compteur(int n, string name);
		int getN();
		void compter();
		void compterV2();
		static int pos;
};