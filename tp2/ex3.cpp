#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <string>
#include "md5.h"

using namespace std;

bool found =false;

void crackCode(string secret, char* letters, int deb, int fin) {
	string word = "aaaaa";
	for(int i=deb; i<fin; i++) {
		if(found == true) {
			return;
		}
       	word[0] = letters[i];
        for(int l2=0; l2<26; l2++) {
        	if(found == true) {
				return;
			}
        	word[1] = letters[l2];
        	for(int l3=0; l3<26; l3++) {
        		if(found == true) {
					return;
				}
        		word[2] = letters[l3];
        		for(int l4=0; l4<26; l4++) {
        			if(found == true) {
    					return;
    				}
        			word[3] = letters[l4];
        			for(int l5=0; l5<26; l5++) {
        				if(found == true) {
        					return;
        				}
        				word[4] = letters[l5];
        				if(secret == md5(word)) {
        					found = true;
        					cout<<"secret trouvé: "<<word<<endl;
        				}
        			}
        		}
        	}
        }
    }
}

int main(int argc, char *argv[]) {
	cout<<md5("zzzzz")<<endl;
	int nbThreads;
	if(argc == 2) {
		nbThreads = atoi(argv[1]);
		if(nbThreads > 26) {
			nbThreads = 26;
		}
	} else {
		nbThreads = 4;
	}

	char letters[26] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	// string secret = "3ed7dceaf266cafef032b9d5db224717";
	string secret = "95ebc3c7b3b9f1d2c40fec14415d3cb8";
	thread threadTable[nbThreads];
	int step = 26 / nbThreads;
	int deb=0;
    int fin=step;
	cout<<"config: "<<step<<", "<<nbThreads<<", step: "<<step<<endl;
	for (int i=0; i<nbThreads; i++) {
		threadTable[i] = thread(crackCode, secret, letters, deb, fin);
		deb+=step;
		if(i == nbThreads-2) {
			fin = 26;
		} else {
			fin+=step;
		}
	}
	for(int i=0; i<nbThreads; i++) {
		threadTable[i].join();
	}
	return 0;
}