#include <iostream>
#include <thread>
#include "matrice.h"

using namespace std;

void hello() {
	cout<<"hello world"<<endl;
}

int main() {
	Matrice A(2, 1, 1);
	A.affiche();
	cout<<"--------------"<<endl;
	Matrice B(1, 2, 2);
	B.affiche();
	cout<<"--------------"<<endl;
	// Matrice AB = A.multSeq(B);
	// AB.affiche();
	// Matrice CD = A.multThread1(A, B);
	Matrice AB2 = A.multThread2(A, B);
	AB2.affiche();
	return 0;
}