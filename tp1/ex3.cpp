#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

mutex val_mutex;
int val=0;
mutex pair_mutex;
mutex impair_mutex;

void pairAffiche() {
    for(int i=0; i<=1000; i+=2) {
        cout<<i<<endl;
    }
}

void imPair() {
    for(int i=1; i<=1000; i+=2) {
        cout<<i<<endl;
    }
}

void mutexPair() {
    for(int i=0; i<=1000; i+=2) {
        pair_mutex.lock();
        cout<<i<<endl;
        impair_mutex.unlock();
    }
}

void mutexImpair() {
    for(int i=1; i<=1000; i+=2) {
        impair_mutex.lock();
        cout<<i<<endl;
        pair_mutex.unlock();
    }
}

int main() {
    /*---
    1)
        Si les deux threads sont lancés en même temps, les nombres ne sont pas dans l'ordre
    ---*/
    /*
    thread p(pairAffiche());
    thread imp(impair);
    p.join();
    imp.join();
    */
    thread p(mutexPair);
    impair_mutex.lock();
    thread imp(mutexImpair);
    p.join();
    imp.join();
	return 0;
}