#include <iostream>
#include <thread>

using namespace std;

void hello() {
	cout<<"hello world"<<endl;
}

int main() {
	thread t(hello);
	thread t2(hello);
	t.join();
	t2.join();
	return 0;
}