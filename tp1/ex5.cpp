#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <stack>

using namespace std;

stack<int> objets;
mutex prodCons_mutex;
int maxObjets;
bool done = false;
int capPile = 10;

void prod(int num) {
    prodCons_mutex.lock();
    cout<<"productuer "<<num<<" crée"<<endl;
    prodCons_mutex.unlock();
    //while(maxObjets>0) {
    while(!done) {
        prodCons_mutex.lock();
        if(objets.size() < capPile && maxObjets > 0) {
            cout<<"("<<num<<") Produit ["<<maxObjets<<"]"<<endl;
            objets.push(maxObjets--);
            if(maxObjets<=0) {
            done =true;
            }
        }
        prodCons_mutex.unlock();
    }
}

void conso(int num) {
    prodCons_mutex.lock();
    cout<<"Consomateur "<<num<<" crée"<<endl;
    prodCons_mutex.unlock();
    while(!done) {
        while(!objets.empty()) {
            prodCons_mutex.lock();
            if(!objets.empty()) {
                cout<<"("<<num<<") Consomme ["<<objets.top()<<"]"<<endl;
                objets.pop();
            }
            prodCons_mutex.unlock();
        }
    }
}

int main(int argc, char** argv) {
    if(argc != 4) {
        return 0;
    }
    int nbObj = atoi(argv[1]);
    int nbProd = atoi(argv[2]);
    int nbCons = atoi(argv[3]);
    cout<<"objs: "<<nbObj<<", nbProd: "<<nbProd<<", nbCons: "<<nbCons<<endl;
    maxObjets = nbObj;

    thread threadProd[nbProd];
    thread threadCons[nbCons];
    for(int i=0; i<nbProd; i++) {
        threadProd[i] = thread(prod, i);
    }
    for(int i=0; i<nbCons; i++) {
        threadCons[i] = thread(conso, i);
    }
    for(int i=0; i<nbCons; i++) {
        threadCons[i].join();
    }
    for(int i=0; i<nbProd; i++) {
        threadProd[i].join();
    }
}