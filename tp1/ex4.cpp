#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

using namespace std;

mutex sum_mutex;

int seqSum(vector<int> T) {
    int sum = 0;
    for(int i=0; i<T.size(); i++) {
        sum+=T[i];
    }
    return sum;
}

void threadLocalSum(vector<int> T, int begin, int end, vector<int>& sumTable, int posSum) {
    for(int i=begin; i<end; i++) {
        sumTable[posSum]+=T[i];
    }
}

int localSum(vector<int> T, int m) {
    if(m>T.size()) {
        m=T.size();
    } else if(m<=0) {
        m=1;
    }
    int sum =0;
    vector<int>  sumTable;
    for(int i=0; i<m; i++) {
        sumTable.push_back(0);
    }
    int nbDiv = T.size() / m;
    cout<<"div: "<<nbDiv<<endl;
    int deb=0;
    int fin=nbDiv;
    thread threadTable[m];
    for(int i=0; i<m; i++) {
        threadTable[i] = thread(threadLocalSum, T, deb, fin, ref(sumTable), i);
        deb+=nbDiv;
        fin+=nbDiv;
    }
    for(int i=0; i<m; i++) {
        threadTable[i].join();
    }
    for(int i=0; i<m; i++) {
        sum+= sumTable[i];
    }
    return sum;
}

void threadSharedSum(vector<int> T, int begin, int end, int& sum) {
    for(int i=begin; i<end; i++) {
        sum+=T[i];
    }
}

int sharedSum(vector<int> T, int m) {
    if(m>T.size()) {
        m=T.size();
    } else if(m<=0) {
        m=1;
    }
    int sum =0;
    int nbDiv = T.size() / m;
    cout<<"div: "<<nbDiv<<endl;
    int deb=0;
    int fin=nbDiv;
    thread threadTable[m];
    for(int i=0; i<m; i++) {
        threadTable[i] = thread(threadSharedSum, T, deb, fin, ref(sum));
        deb+=nbDiv;
        fin+=nbDiv;
    }
    for(int i=0; i<m; i++) {
        threadTable[i].join();
    }
    return sum;
}

int main() {
    vector<int>  T;
    for(int i=0; i<6; i++) {
        T.push_back(1);
    }
    cout<<"seqSum: "<<seqSum(T)<<endl;
    cout<<"sumLocal: "<<localSum(T, 3)<<endl;
    cout<<"SumShared: "<<sharedSum(T, 3)<<endl;
    return 0; 
}