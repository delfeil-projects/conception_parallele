#include <iostream>
#include <thread>
#include "matrice.h"

Matrice::Matrice(int nb_l, int nb_c, int val) {
	nb_ligne=nb_l;
	nb_col=nb_c;
	// table[nb_ligne][nb_col];
	table = new int *[nb_ligne];
	for(int i=0; i<nb_ligne; i++) {
		table[i] = new int[nb_col];
		for(int j=0; j<nb_col; j++) {
			table[i][j] = val;
		}
	}
}

Matrice Matrice::multSeq(Matrice B) {
	Matrice AB(1, 1, 0);
	if(nb_col == B.getNbLigne()) {
		AB = Matrice(nb_ligne, B.getNbCol(), 0);
		for(int i=0; i<nb_ligne; i++) {
			for(int j=0; j<B.getNbCol(); j++) {
				int val =0;
				for(int k=0; k<nb_col; k++) {
					val += table[i][k] * B.getVal(k, j);
				}
				AB.setVal(i, j, val);
			}
		}
	} else {
		cout<<"Dimensions de matrices à multiplier invalide"<<endl;
	}
	return AB;
}

void calcVal(Matrice A, Matrice B, Matrice AB, int i, int j) {
	cout<<"thread:val "<<i<<", "<<j<<endl;
	int val =0;
	for(int k=0; k<A.getNbCol(); k++) {
		val += A.getVal(i, k) * B.getVal(k, j);
	}
	AB.setVal(i, j, val);
}

Matrice Matrice::multThread1(Matrice A, Matrice B) {
	Matrice AB(1, 1, 0);
	if(nb_col == B.getNbLigne()) {
		AB = Matrice(nb_ligne, B.getNbCol(), 0);
		for(int i=0; i<nb_ligne; i++) {
			for(int j=0; j<B.getNbCol(); j++) {
				thread calc(calcVal, A, B, AB, i, j);
				calc.join();
			}
		}
	} else {
		cout<<"Dimensions de matrices à multiplier invalide"<<endl;
	}
	return AB;
}

void calcLigne(Matrice A, Matrice B, Matrice AB, int i) {
	cout<<"thread:ligne "<<i<<endl;
	for(int j=0; j<B.getNbCol(); j++) {
		int val =0;
		for(int k=0; k<A.getNbCol(); k++) {
			val += A.getVal(i, k) * B.getVal(k, j);
		}
		AB.setVal(i, j, val);
	}
}

Matrice Matrice::multThread2(Matrice A, Matrice B) {
	Matrice AB(1, 1, 0);
	if(nb_col == B.getNbLigne()) {
		AB = Matrice(nb_ligne, B.getNbCol(), 0);
		for(int i=0; i<nb_ligne; i++) {
			thread calc(calcLigne, A, B, AB, i);
			calc.join();
		}
	} else {
		cout<<"Dimensions de matrices à multiplier invalide"<<endl;
	}
	return AB;
}

void Matrice::setVal(int num_lign, int num_col, int val) {
	table[num_lign][num_col] = val;
}

int Matrice::getVal(int num_lign, int num_col) {
	return table[num_lign][num_col];
}

int Matrice::getNbLigne() {
	return nb_ligne;
}

int Matrice::getNbCol() {
	return nb_col;
}

void Matrice::affiche() {
	for(int i=0; i<nb_ligne; i++) {
		for(int j=0; j<nb_col; j++) {
			cout<<table[i][j];
		}
		cout<<endl;
	}
}