#include <iostream>

using namespace std;


class Matrice {
	int nb_ligne;
	int nb_col;
	int** table;

	public:
		Matrice(int nb_l, int nb_c, int val);
		Matrice multSeq(Matrice B);
		Matrice multThread1(Matrice A, Matrice B);
		Matrice multThread2(Matrice A, Matrice B);
		void affiche();
		int getNbLigne();
		int getNbCol();
		int getVal(int num_lign, int num_col);
		void setVal(int num_lign, int num_col, int val);
};

void calcVal(Matrice A, Matrice B, Matrice AB, int i, int j);
void calcLigne(Matrice A, Matrice B, Matrice AB, int i);